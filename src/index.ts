'use strict'
/**
 * BEGIN HEADER
 *
 * Contains:        Utility function
 * CVM-Role:        <none>
 * Maintainer:      Matt Jolly
 * License:         GNU GPL v3
 *
 * Description:     This module produces a Markdown table from an input object.
 *
 * END HEADER
 */

import { makeGridTable, makeMultilineTable, makePipeTable, makeSimpleTable } from './util/tableGenerators'
import { Alignment } from './util/tableHelpers'

export interface TableContent {
  columns: Columns
  rows: Row[]
  caption?: string
  type?: 'grid' | 'gfm' | 'pipe' | 'simple' | 'multiline'
}

export type Row = Array<string | number | undefined | null>
export interface Columns {
  [x: string]: {
    alignment?: Alignment
  }
}

/**
 * @param {object} content  Table Content
 * @returns {string}        A string containing a Markdown table.
 */
export default function makeTable(content: TableContent): string {
  let table = ''
  const type = content.type ? content.type : 'grid'

  switch (type) {
    case 'grid':
      table = makeGridTable(content)
      break
    case 'multiline':
      table = makeMultilineTable(content)
      break
    case 'gfm':
    case 'pipe':
      table = makePipeTable(content)
      break
    default:
      table = makeSimpleTable(content)
      break
  }
  return table
}
