/**
 * BEGIN HEADER
 *
 * Contains:        Utility function
 * CVM-Role:        <none>
 * Maintainer:      Matt Jolly
 * License:         GNU GPL v3
 *
 * Description:     This module contains table helper functions.
 *
 * END HEADER
 */

import { TableContent, Columns } from '../index'
import isEmpty from './isEmpty'
import { EOL } from 'os'

/**
 * @param   {string} type   Table Type
 * @returns {string}        Column Separator
 */
export function getColumnSeparator(type: string): string {
  let columnSep = ''
  switch (type) {
    case 'gfm':
      columnSep = '|'
      break
    case 'grid':
      columnSep = '|'
      break
    case 'simple':
      columnSep = ' '
      break
    case 'multiline':
      columnSep = ' '
      break
  }
  return columnSep
}

/**
 * A helper function to get row widths. Will only count the longest span if wrapped.
 *
 * This counts content only; add any padding to your calculations!
 *
 * @param   {object}    content     Table Content
 * @returns {number[]}              An array of column widths.
 */
export function getColumnWidths(content: TableContent): number[] {
  const columnWidths: number[][] = []
  const headingWidths: number[] = []
  let columnCount = 0
  for (const column in content.columns) {
    columnCount++
    headingWidths.push(column.length)
  }
  for (const row of content.rows) {
    const cellWidthArr: number[] = []
    for (let i = 0; i < columnCount; i++) {
      let contentLength = 0
      if (!isEmpty(row[i])) {
        if (typeof row[i] === 'string') {
          if ((row[i] as string).match(/\r\n/g)) {
            const splitContent = (row[i] as string).split('\r\n')
            contentLength = Math.max(
              ...splitContent.map(function (el) {
                return el.length
              })
            )
          } else if ((row[i] as string).match(/\n/g)) {
            const splitContent = (row[i] as string).split('\n')
            contentLength = Math.max(
              ...splitContent.map(function (el) {
                return el.length
              })
            )
          } else {
            contentLength = (row[i] as string).length
          }
        } else if (typeof row[i] === 'number') {
          contentLength = (row[i] as number).toString().length
        }
      } else {
        contentLength = 15 // Some row values may be undefined/null. Set a sane value just in case.
      }
      cellWidthArr.push(contentLength)
    }
    columnWidths.push(cellWidthArr)
  }
  const maxContentWidths = columnWidths.reduceRight((r, a) => r.map((b, i) => (b && Math.max(b, a[i])) || 0))
  return maxContentWidths.map((item, i) => Math.max(item, headingWidths[i]))
}

/**
 * @param   {Array}  columnWidths  An array containing the width of text for each row.
 * @returns {string}            A grid table row separator.
 */
export function makeGridRowSep(columnWidths: number[]): string {
  const rowArray: string[] = []
  columnWidths.forEach(function (item) {
    rowArray.push('+-')
    rowArray.push(new Array(item + 1).join('-'))
    rowArray.push('-')
  })
  rowArray.push('+')
  return rowArray.join('')
}

/**
 * @param   {Array}  columnWidths  An array containing the width of text for each row.
 * @returns {string}            A Multiline/Simple table heading separator.
 */
export function makeDashHeadingSep(columnWidths: number[]): string {
  const rowArray: string[] = []
  columnWidths.forEach(function (item, index, array) {
    rowArray.push(new Array(item + 1).join('-'))
    if (index !== array.length - 1) rowArray.push(' ')
  })
  return rowArray.join('')
}

/**
 * A function to make a Grid Table heading separator row (with alignments).
 *
 * @param {Array}   columnWidths Array containing widths of row content.
 * @param {Object}  columns    Object containing column names and alignment.
 * @returns {string}  A string containing the heading row separator for a grid table.
 */
export function makeGridHeadingSep(columnWidths: number[], columns: Columns): string {
  let headingSep = ''
  const headingArr: string[] = []
  let columnCount = 0
  for (const column in columns) {
    headingArr.push('+')
    switch (columns[column].alignment) {
      case 'centre' || 'center':
        headingArr.push(`:${new Array(columnWidths[columnCount] + 1).join('=')}:`)
        break
      case 'left':
        headingArr.push(`:${new Array(columnWidths[columnCount] + 1).join('=')}=`)
        break
      case 'right':
        headingArr.push(`=${new Array(columnWidths[columnCount] + 1).join('=')}:`)
        break
      default:
        headingArr.push(`=${new Array(columnWidths[columnCount] + 1).join('=')}=`)
        break
    }
    columnCount++
  }
  headingArr.push('+')
  headingSep = headingArr.join('')
  return headingSep
}

/**
 * A function to make a GFM / Pipe Table heading separator row (with alignments)
 *
 * @param {Array}   columnWidths Array containing widths of row content.
 * @param {Object}  columns    Object containing column names and alignment.
 * @returns {string}  A string containing the heading row separator for a grid table.
 */
export function makePipeHeadingSep(columnWidths: number[], columns: Columns): string {
  const headingArr: string[] = []
  let columnCount = 0
  for (const column in columns) {
    headingArr.push('|')
    switch (columns[column].alignment) {
      case 'centre' || 'center':
        headingArr.push(`:${new Array(columnWidths[columnCount] + 1).join('-')}:`)
        break
      case 'left':
        headingArr.push(`:${new Array(columnWidths[columnCount] + 1).join('-')}-`)
        break
      case 'right':
        headingArr.push(`-${new Array(columnWidths[columnCount] + 1).join('-')}:`)
        break
      default:
        headingArr.push(`-${new Array(columnWidths[columnCount] + 3).join('-')}-`)
        break
    }
    columnCount++
  }
  headingArr.push('|')
  return headingArr.join('')
}

type CellContent = string | number | null | undefined
export type Alignment = 'left' | 'right' | 'center' | 'centre' | 'default'

/**
 * A Function to place cell content for single-line table types
 * by padding with appropriate whitespace. If you need a cell delimiter
 * it's assumed that your function will include it and any padding required.
 *
 * @param {CellContent}   cellContent  Content of the cell
 * @param {number}        columnWidth  Width of the column between cell seperators
 * @param {string}        alignment    Alighment of content within the cell
 * @returns {string}                   A string containing a row of markdown table content with appropriate padding
 */
export function placeCellContentWithPadding(cellContent: CellContent, columnWidth: number, alignment?: Alignment): string {
  const paddedCell = []
  if (isEmpty(cellContent)) {
    paddedCell.push(new Array(columnWidth + 1).join(' '))
  } else {
    let contentWidth: number
    if (typeof cellContent === 'number') {
      contentWidth = cellContent.toString().length
    } else if (typeof cellContent === 'undefined' || cellContent === null) {
      contentWidth = 0
    } else {
      contentWidth = cellContent.length
    }
    switch (alignment) {
      case 'right':
        if (columnWidth - contentWidth > 0) {
          paddedCell.push(new Array(columnWidth - contentWidth + 1).join(' '))
        }
        paddedCell.push((cellContent as string).toString())
        break
      case 'centre':
      case 'center':
        if (columnWidth - contentWidth > 0) {
          paddedCell.push(new Array(Math.floor((columnWidth - contentWidth) / 2) + 1).join(' '))
        }
        paddedCell.push((cellContent as string).toString())
        if (columnWidth - contentWidth > 0) {
          paddedCell.push(new Array(Math.ceil((columnWidth - contentWidth) / 2) + 1).join(' '))
        }
        //BUG: This seems to always be up by one padding character on the right...
        break
      default:
        paddedCell.push((cellContent as string).toString())
        if (columnWidth - contentWidth > 0) {
          paddedCell.push(new Array(columnWidth - contentWidth + 1).join(' '))
        }
        break
    }
  }
  return paddedCell.join('')
}

type CellSeparator = ' ' | '|'

/**
 * A Function to place cell content within the required number of rows for multiline table types
 * by padding with appropriate whitespace.
 *
 * @param {Array<CellContent>}  rowContent    Content of the row that needs to be parsed
 * @param {number[]}            columnWidths  Width of the column between cell seperators
 * @param {CellSeparator}  cellSep       The separator that your table uses
 * @param {string}              alignment     alignment of content within the cell
 * @returns {string}                   A string containing a row of markdown table content with appropriate padding
 */
export function placeRowContent(
  rowContent: CellContent[],
  columnWidths: number[],
  cellSep: CellSeparator,
  alignment: Array<Alignment | undefined>
): string {
  const rawContentMarkdown: string[] = []
  const linesArr: number[] = []

  rowContent.forEach((cellContent) => {
    if (!isEmpty(cellContent) && typeof cellContent === 'string') {
      linesArr.push(cellContent.split(/\r\n|\r|\n/).length)
    } else {
      linesArr.push(1)
    }
  })
  const numberOfLines: number = Math.max(...linesArr)

  for (let lineCount = 0; lineCount < numberOfLines; lineCount++) {
    for (let columnCount = 0; columnCount < rowContent.length; columnCount++) {
      if (cellSep === '|') {
        columnCount === 0 ? rawContentMarkdown.push(`${cellSep} `) : rawContentMarkdown.push(` ${cellSep} `)
      } else if (columnCount !== 0) {
        rawContentMarkdown.push(cellSep)
      }
      let cellContent = ''
      if (typeof rowContent[columnCount] === 'string') {
        if (/\r\n|\r|\n/.exec(rowContent[columnCount] as string)) {
          cellContent = placeCellContentWithPadding(
            (rowContent[columnCount] as string).split(/\r\n|\r|\n/)[lineCount],
            columnWidths[columnCount],
            alignment[columnCount]
          )
        } else if (lineCount > 0) {
          cellContent = new Array(columnWidths[columnCount] + 1).join(' ')
        } else {
          cellContent = placeCellContentWithPadding(rowContent[columnCount], columnWidths[columnCount], alignment[columnCount])
        }
      } else {
        cellContent = placeCellContentWithPadding(rowContent[columnCount], columnWidths[columnCount], alignment[columnCount])
      }
      if (columnCount < rowContent.length - 1) {
        rawContentMarkdown.push(cellContent)
      } else if (isEmpty(rowContent[columnCount + 1])) {
        // This should cover the case if we're at the end of a row or the cell is "empty"
        switch (cellSep) {
          case '|':
            rawContentMarkdown.push(cellContent)
            break
          case ' ':
            rawContentMarkdown.push(cellContent.trimEnd())
            break
        }
      }
    }

    if (cellSep === '|') {
      lineCount < numberOfLines - 1 ? rawContentMarkdown.push(` ${cellSep}${EOL}`) : rawContentMarkdown.push(` ${cellSep}`)
    } else {
      if (lineCount < numberOfLines - 1) rawContentMarkdown.push(EOL)
    }
  }
  return rawContentMarkdown.join('')
}
