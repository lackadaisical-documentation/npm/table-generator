# Lackadaisical Table Generator

Takes an input object and returns a string containing a Markdown table. Supports all Pandoc-Markdown table types: Pipe/GFM, Simple, Multiline, and Grid/RST

## Usage

```typescript

import makeTable from '@lackadaisical/table-generator'

const tableContent TableContent = {
  columns: {
    column1: {
      alignment: 'left',
    },
    column2: {
      alignment: 'right',
    },
    'column 3': {
      alignment: 'centre',
    },
  },
  rows: [
    ['this', 'is', 'one row'],
    ['this', 'is', 'a second row'],
    ['this', 'is', 'another row that is really really really long'],
  ],
  caption: 'Test Table!',
  type: 'pipe',
}

table: string = makeTable(tableContent)
```
