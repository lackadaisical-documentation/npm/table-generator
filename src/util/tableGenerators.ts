/**
 * BEGIN HEADER
 *
 * Contains:        Utility function
 * CVM-Role:        <none>
 * Maintainer:      Matt Jolly
 * License:         GNU GPL v3
 *
 * Description:     This file contains functions to generate a string containing a Markdown table.
 *
 * END HEADER
 */

import { EOL } from 'os'

import {
  Alignment,
  getColumnWidths,
  makeDashHeadingSep,
  makeGridHeadingSep,
  makeGridRowSep,
  makePipeHeadingSep,
  placeRowContent,
} from './tableHelpers'

import { TableContent, Row } from '../index'

/**
 * Makes a grid table.
 *
 * @param   {Object}  content An object containing the content you want a table from.
 * @returns {string}          A grid table and optional caption.
 */
export function makeGridTable(content: TableContent): string {
  const columnWidths: number[] = getColumnWidths(content)
  const cellSep = '|'
  const footerRow = EOL + makeGridRowSep(columnWidths) + EOL
  const headerRow = makeGridRowSep(columnWidths) + EOL
  const headingSep = EOL + makeGridHeadingSep(columnWidths, content.columns) + EOL
  const rowSep = EOL + makeGridRowSep(columnWidths) + EOL
  const tableArray: string[] = []
  const alignmentArr: Array<Alignment | undefined> = []
  const headingRowArr: Row = []

  tableArray.push(headerRow)

  for (const column in content.columns) {
    alignmentArr.push(content.columns[column].alignment)
    headingRowArr.push(column)
  }

  // Heading row content placement; content always centred
  tableArray.push(placeRowContent(headingRowArr, columnWidths, cellSep, Array(alignmentArr.length).fill('center') as Alignment[]))

  tableArray.push(headingSep)

  content.rows.forEach((element, index, array) => {
    tableArray.push(placeRowContent(element, columnWidths, cellSep, alignmentArr))
    index !== array.length - 1 ? tableArray.push(rowSep) : tableArray.push(footerRow)
  })

  if (!(typeof content.caption === 'undefined')) tableArray.push(`${EOL}:${content.caption}${EOL}`)

  return tableArray.join('')
}

/**
 * Makes a Multiline Markdown table.
 *
 * @param   {Object}  content An object containing the content you want a table from.
 * @returns {string}          A grid table and optional caption.
 */
export function makeMultilineTable(content: TableContent): string {
  const tableArray: string[] = []
  // Two additional spaces are used to pad out each cell as column alignment is dependent on alignment of the heading rather than content, if a heading has been included.
  const columnWidths: number[] = getColumnWidths(content).map((x) => x + 2)
  const cellSep = ' '
  const footerRow = EOL + new Array(columnWidths.reduce((a, b) => a + b, 0) + columnWidths.length).join('-') + EOL
  const headerRow = new Array(columnWidths.reduce((a, b) => a + b, 0) + columnWidths.length).join('-') + EOL
  const headingSep = makeDashHeadingSep(columnWidths)
  const rowSep = EOL + EOL
  const alignmentArr: Array<Alignment | undefined> = []
  const headingRowArr: Row = []

  tableArray.push(headerRow)
  for (const column in content.columns) {
    alignmentArr.push(content.columns[column].alignment)
    headingRowArr.push(column)
  }

  tableArray.push(placeRowContent(headingRowArr, columnWidths, cellSep, alignmentArr))

  tableArray.push(EOL)
  tableArray.push(headingSep, EOL)

  content.rows.forEach((element, index, array) => {
    tableArray.push(placeRowContent(element, columnWidths, cellSep, alignmentArr))
    index !== array.length - 1 ? tableArray.push(rowSep) : tableArray.push(footerRow)
  })
  if (!(typeof content.caption === 'undefined')) tableArray.push(`${EOL}:${content.caption}${EOL}`)
  return tableArray.join('')
}

/**
 * Makes a Pipe / Github-Flavoured Markdown table.
 *
 * @param   {TableContent}  content   An object containing the content you want a table from.
 * @returns {string}                  A grid table and optional caption.
 */
export function makePipeTable(content: TableContent): string {
  const columnWidths: number[] = getColumnWidths(content)
  const cellSep = '|'
  const rowSep = EOL
  const headingSep = EOL + makePipeHeadingSep(columnWidths, content.columns) + EOL
  const tableArray: string[] = []
  const alignmentArr: Array<Alignment | undefined> = []
  const headingRowArr: Row = []

  for (const column in content.columns) {
    alignmentArr.push(content.columns[column].alignment)
    headingRowArr.push(column)
  }

  // Heading row content placement; content always centred
  tableArray.push(placeRowContent(headingRowArr, columnWidths, cellSep, Array(alignmentArr.length).fill('center') as Alignment[]))

  tableArray.push(headingSep)

  content.rows.forEach((element, index, array) => {
    tableArray.push(placeRowContent(element, columnWidths, cellSep, alignmentArr))
    index !== array.length - 1 ? tableArray.push(rowSep) : tableArray.push(EOL)
  })

  if (!(typeof content.caption === 'undefined')) tableArray.push(`${EOL}:${content.caption}${EOL}`)

  return tableArray.join('')
}

/**
 * Makes a Simple Markdown table.
 *
 * @param   {Object}  content An object containing the content you want a table from.
 * @returns {string}          A grid table and optional caption.
 */
export function makeSimpleTable(content: TableContent): string {
  const tableArray: string[] = []
  // Two additional spaces are used to pad out each cell as column alignment is dependent on alignment of the heading rather than content, if a heading has been included.
  const columnWidths: number[] = getColumnWidths(content).map((x) => x + 2)

  const cellSep = ' '
  const footerRow = EOL + makeDashHeadingSep(columnWidths) + EOL
  const headingSep = makeDashHeadingSep(columnWidths) + EOL
  const rowSep = EOL
  const headingRowArr: Row = []

  const alignmentArr: Array<Alignment | undefined> = []

  for (const column in content.columns) {
    alignmentArr.push(content.columns[column].alignment)
    headingRowArr.push(column)
  }

  tableArray.push(placeRowContent(headingRowArr, columnWidths, cellSep, alignmentArr))

  tableArray.push(EOL)
  tableArray.push(headingSep)

  content.rows.forEach((element, index, array) => {
    tableArray.push(placeRowContent(element, columnWidths, cellSep, alignmentArr))
    index !== array.length - 1 ? tableArray.push(rowSep) : tableArray.push(footerRow)
  })

  if (!(typeof content.caption === 'undefined')) tableArray.push(`${EOL}:${content.caption}${EOL}`)
  return tableArray.join('')
}
