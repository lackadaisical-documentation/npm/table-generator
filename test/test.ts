import test, { Macro } from 'ava'
import makeTable, { TableContent } from '../src/index'
import fs from 'fs'
import path from 'path'
import { EOL } from 'os'
import isEmpty from '../src/util/isEmpty'

process.chdir('./test')

const singleLineContent: TableContent = {
  columns: {
    column1: {
      alignment: 'left',
    },
    column2: {
      alignment: 'right',
    },
    'column 3': {
      alignment: 'centre',
    },
  },
  rows: [
    ['this', 'is', 'one row'],
    ['this', 'is', 'second row'],
    ['this', 'is', 'another row that is really really really long'],
  ],
  caption: 'Test Table!',
}

const multiLineContent: TableContent = {
  columns: {
    column1: {
      alignment: 'left',
    },
    column2: {
      alignment: 'right',
    },
    'column 3': {
      alignment: 'centre',
    },
  },
  rows: [
    ['this', 'is', 'one row'],
    ['this', 'is', 'second row'],
    ['this', 'is', 'another row\nthat breaks'],
  ],
  caption: 'Test Table!',
}

type Type = 'grid' | 'gfm' | 'pipe' | 'simple' | 'multiline' | undefined

// Table Generation Test Macro:
/**
 * @param {string}  t         Test title
 * @param {string}  type      Type of table we want to generate
 * @param {string}  expected  Path to test results
 */
const testMacroSimpleTable: Macro<[Type, string]> = (t, type: Type, expected) => {
  const table = singleLineContent
  if (type !== undefined) {
    table.type = type
  }
  t.is(makeTable(table), fs.readFileSync(path.resolve(expected), 'utf8').replace(/\r\n|\r|\n/, EOL))
}

// eslint-disable-next-line @typescript-eslint/restrict-template-expressions
testMacroSimpleTable.title = (providedTitle = 'Test Simple table generation for type:', type: Type) => `${providedTitle} '${type}'`

test(testMacroSimpleTable, undefined, './test_results/test_simple_grid_table')
test(testMacroSimpleTable, 'grid', './test_results/test_simple_grid_table')
test(testMacroSimpleTable, 'pipe', './test_results/test_simple_pipe_table')
test(testMacroSimpleTable, 'multiline', './test_results/test_simple_multiline_table')
test(testMacroSimpleTable, 'simple', './test_results/test_simple_simple_table')

// isEmpty Test Macro:
/**
 * @param {string}  t         Test title
 * @param {?}       value     Type of table we want to generate
 */
const testMacroisEmpty: Macro<[unknown]> = (t, value: unknown) => {
  t.is(isEmpty(value), true)
}

// eslint-disable-next-line @typescript-eslint/restrict-template-expressions
testMacroisEmpty.title = (providedTitle = "`isEmpty` returns 'true' for:", value: unknown) => `${providedTitle} '${value}' as ${typeof value}`

test(testMacroisEmpty, undefined)
test(testMacroisEmpty, false)
test(testMacroisEmpty, null)
test(testMacroisEmpty, '')
test(testMacroisEmpty, '0')
test(testMacroisEmpty, 0)
test(testMacroisEmpty, {})
test(testMacroisEmpty, [])

test('Generates a valid Multiline Grid table when no table type is requested', (t) => {
  const result = makeTable(multiLineContent)
  const stored_value = fs.readFileSync(path.resolve('./test_results/test_grid_table_multiline'), 'utf8').replace(/\r\n|\r|\n/, EOL)
  t.is(result, stored_value)
})

test('Generates a valid multiline Multiline table when multiline type requested and given multiline input', (t) => {
  const table = multiLineContent
  table.type = 'multiline'
  const result = makeTable(table)
  const stored_value = fs.readFileSync(path.resolve('./test_results/test_multiline_table_multiline'), 'utf8').replace(/\r\n|\r|\n/, EOL)
  t.is(result, stored_value)
})

test('Properly pads empty cells', (t) => {
  const table: TableContent = {
    columns: {
      column1: {
        alignment: 'left',
      },
      column2: {
        alignment: 'right',
      },
      'column 3': {
        alignment: 'centre',
      },
    },
    rows: [
      [null, 'false', 'one row'],
      ['this', undefined, 'second row'],
      ['this', 'is', 'another row\nthat breaks'],
    ],
    type: 'grid',
    caption: 'Test Table!',
  }
  const result = makeTable(table)
  const stored_value = fs.readFileSync(path.resolve('./test_results/test_grid_table_multiline_with_empty'), 'utf8').replace(/\r\n|\r|\n/, EOL)
  t.is(result, stored_value)
})

test.todo('Simple tables can be created with no headings')
test.todo('Multiline tables can be created with no headings')
test.todo('Pipe tables can be created with no headings')
