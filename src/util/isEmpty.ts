/**
 * BEGIN HEADER
 *
 * Contains:        Utility function
 * CVM-Role:        <none>
 * Maintainer:      Matt Jolly
 * License:         GNU GPL v3
 *
 * Description:     This utility function determines whether or not a variable is 'empty'
 *
 * END HEADER
 */

/**
 * Function to determine whether or not a variable is empty.
 *
 * Considers null, undefined, '', false, 0, 0.0, '0', {}, [] to be empty.
 *
 * @param   {*}       value Variable that we want to know about.
 * @returns {boolean}       A boolean value.
 */
export default function isEmpty(value: unknown): boolean {
  const type = typeof value
  if (type === 'undefined') {
    return true
  }
  if (type === 'boolean') {
    return !value
  }
  if (value === null) {
    return true
  }
  if (value === undefined) {
    return true
  }
  if (value instanceof Array) {
    if (value.length < 1) {
      return true
    }
  } else if (typeof value === 'string') {
    if (value.length < 1) {
      return true
    }
    if (value === '0') {
      return true
    }
  } else if (typeof value === 'object') {
    if (Object.keys(value as Record<string, unknown>).length < 1) {
      return true
    }
  } else if (type === 'number') {
    if (value === 0) {
      return true
    }
  }
  return false
}
